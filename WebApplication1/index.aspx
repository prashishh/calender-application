<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="960.css" media="screen" />
<link rel="stylesheet" type="text/css" href="navig.css" media="screen" />
<link rel="stylesheet" type="text/css" href="default.css" media="screen" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />


<script type="text/javascript" src="Scripts/jquery-1.4.1.js"></script>

  
	<script type="text/javascript">


	    $(document).ready(function () {

	        $(".inputDefault").focus(function (srcc) {
	            if ($(this).val() == $(this).attr('title')) {
	                $(this).removeClass("inputDefaultActive");
	                $(this).val("");
	            }
	        });

	        $(".inputDefault").blur(function () {
	            if ($(this).val() == "") {
	                $(this).addClass("inputDefaultActive");
	                $(this).val($(this).attr('title'));
	            }
	        });

	        $(".inputDefault").focus();
	        $(".inputDefault").blur();

	        $(".ping").click(function () {
	            alert("hello");
	        });

	    });
   
	</script>


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />

<script type="text/javascript">


	function Navigate() {
		var newurl = $('#ifrmSite').val();
		$('#iframe1').attr('src', newurl);
		window.frames["iframe1"].location.reload();
	}

	function SumbitBut1_onclick() {

	}

</script>

</head> 
<body>

<div id="universalcontainer">

<div><img alt="msdn" src="bg_head.jpg" /></div>

<div class="container_12 container">

<div class = "grid_12 title">
	<h2>Unlimited Panchanga Rupantar</h2>

	<div class = "clear"></div>
	<div class = "clear"></div>

	<p>Bikram Sambat-AD Date Converter and Calender</p>

		<div class = " rid_12">

		<div class = "grid_6 alpha"> 
		AD:
		<div class="clear"></div>
		<br />
			<form id="form1" runat="server">
				<div class = "grid_1 alpha barcont">

				<asp:DropDownList ID="engdate" runat="server" class="inputContainer" >
					 
				</asp:DropDownList>

				
				</div>
				<div class = "grid_2 alpha inputContainer">

				 <asp:DropDownList ID="engmonth" runat="server"  
						class="inputContainer monthtext" >
					<asp:ListItem Value="1">January</asp:ListItem>
					<asp:ListItem Value="2">February</asp:ListItem>
					<asp:ListItem Value="3">March</asp:ListItem>
					<asp:ListItem Value="4">April</asp:ListItem>
					<asp:ListItem Value="5">May</asp:ListItem>
					<asp:ListItem Value="6">June</asp:ListItem>
					<asp:ListItem Value="7">July</asp:ListItem>
					<asp:ListItem Value="8">August</asp:ListItem>
					<asp:ListItem Value="9">September</asp:ListItem>
					<asp:ListItem Value="10">October</asp:ListItem>
					<asp:ListItem Value="11">November</asp:ListItem>
					<asp:ListItem Value="12">December</asp:ListItem>
					
				</asp:DropDownList>
				</div>
				<div class = "grid_1 omega inputContainer">
					 <asp:DropDownList ID="engyear" runat="server" class="inputContainer" >
		
				</asp:DropDownList>
				</div>

				<div class = "clear"></div>

				<div class = "submit grid_4 push_2">
					<asp:Button ID="engsumbit"  class = "grid_2" runat="server" onclick="Submit_Click2" Text="Convert to BS" />
				</div>

			   
		</div>

		<div class = "grid_6 omega">
		Bikram Sambat:
		<div class="clear"></div>
		<br />
 
 
				<div class = "grid_1 alpha">
				<asp:DropDownList ID="nepdate" runat="server" class="inputContainer" >
					 
				</asp:DropDownList>
				</div>
				<div class = "grid_2 alpha inputContainer">

				<asp:DropDownList ID="nepmonth" runat="server"  
						class="inputContainer monthtext" >
					<asp:ListItem Value="1">Baishakh</asp:ListItem>
					<asp:ListItem Value="2">Jestha</asp:ListItem>
					<asp:ListItem Value="3">Asadh</asp:ListItem>
					<asp:ListItem Value="4">Shrawan</asp:ListItem>
					<asp:ListItem Value="5">Bhadra</asp:ListItem>
					<asp:ListItem Value="6">Aswin</asp:ListItem>
					<asp:ListItem Value="7">Kartik</asp:ListItem>
					<asp:ListItem Value="8">Mangsir</asp:ListItem>
					<asp:ListItem Value="9">Poush</asp:ListItem>
					<asp:ListItem Value="10">Magh</asp:ListItem>
					<asp:ListItem Value="11">Falgun</asp:ListItem>
					<asp:ListItem Value="12">Chaitra</asp:ListItem>
					
				</asp:DropDownList>
					
				</div>
				<div class = "grid_1 omega inputContainer">
					 <asp:DropDownList ID="nepyear" runat="server" class="inputContainer" >
					 
				</asp:DropDownList>
				</div>

	   <div class = "clear"></div>

				<div class = "submit grid_4 push_2">
					<asp:Button ID="nepsubmit" value ="nepsubmit"  class = "grid_2" runat="server" onclick="Submit_Click" Text="Convert to AD" />
				</div>

		</div>
		 </form>  
		</div>
</div>

<div class = "resultpg2 grid_12">
<asp:Label ID="rslt" runat="server" class = "grid_4 push_4"></asp:Label>
<div class="clear"></div>
<% if (IsPostBack == true)
   { %>
<asp:Label ID="rslt2" runat="server" class = "grid_4 push_4 reference ">(YYYY/MM/DD)</asp:Label>
<div class="clear"></div>
<% } %>
<asp:Label ID="error" runat="server" class = "grid_4 push_4"></asp:Label>
<div class="clear"></div>

</div>
<% if (IsPostBack == true && button1 == 1)
   {
    
     %>
<div class ="calr">

<div class = "nepmonth grid_12">
    <div class ="alpha grid_2"><% Response.Write(GetEngMonthD(nepmonth.SelectedIndex) + "(" + nepyear.SelectedValue.ToString() + ")"); %></div>
    <div class ="omega grid_2 push_7"><% Response.Write(EngMonthMap[(GetEngMonthD(nepmonth.SelectedIndex))]); %></div>

</div>
                        <ol class="calendar" start = "6">
                        <li id='lastmonth'>
                        <ol>
                            <li><p class="datetext day">Sun</p></li>
                            <li><p class="datetext day">Mon</p></li>
                            <li><p class="datetext day">Tue</p></li>
                            <li><p class="datetext day">Wed</p></li>
                            <li><p class="datetext day">Thur</p></li>
                            <li><p class="datetext day">Fri</p></li>
                            <li><p class="datetext holiday">Sat</p></li>
                        </ol>
                        </li>
                    <%
                        
                       
                                         int month = getEngMonth();
                                         int year = getEngYear();
                                         int totaldays = getEngTotalDay(); ;
                                         int day = 1;
                                         int date = getEngDay(month, year);

                                         int ft = 0;
                                         if (date != 0 && totaldays != 0 && month != 0)
                                         {
                                             for (int i = 1; i <= 6; i++)
                                             {
                                                 for (int j = 1; j <= 7; )
                                                 {
                                                     if (ft == 0)
                                                     {
                                                         Response.Write("<li id='lastmonth'>");
                                                         Response.Write("<ol>");
                                                         for (int k = 1; k < date; k++)
                                                         {
                                                             Response.Write("<li>" + "\t" + "</li>");
                                                             j++;
                                                         }
                                                         Response.Write("</ol>");
                                                         Response.Write("</li>");
                                                         ft = 1;
                                                     }
                                                     Response.Write("<li id='thismonth'>");
                                                     Response.Write("<ol>");

                                                     if (day <= totaldays)
                                                     {
                                                         if (day != Convert.ToInt32(nepdate.SelectedItem.ToString()))
                                                         {
                                                             Response.Write("<li id ='" + getEngYear() + "' class = 'ping'>" + getCorresEngDate(day) + "<p class = 'datetext'>" + NepDateMap[day] + "</p></li>");
                                                             day++;
                                                         }
                                                         else
                                                         {
                                                             Response.Write("<li id ='" + getEngYear() + "' class = 'ping thatdate'>" + getCorresEngDate(day) + "<p class = 'datetext'>" + NepDateMap[day] + "</p></li>");
                                                             day++;
                                                         }
                                                     }
                                                     j++;
                                                     Response.Write("</ol>");
                                                 }


                                                 Response.Write("</li>");
                                             }
                                         }
                    %>
                </ol>

                </div>  
                <%
                    button1 = 0;
                    } %>
              
                    
                  <% if (IsPostBack == true && button2 == 1)
   {
    
     %>
<div class ="calr">

<div class = "nepmonth grid_12">
    <div class ="alpha grid_2"><% Response.Write(GetEngMonthD(nepmonth.SelectedIndex) + "(" + nepyear.SelectedValue.ToString() + ")"); %></div>
    <div class ="omega grid_2 push_7"><% Response.Write(EngMonthMap[(GetEngMonthD(nepmonth.SelectedIndex))]); %></div>

</div>
                        <ol class="calendar" start = "6">
                        <li id='Li1'>
                        <ol>
                            <li><p class="datetext day">Sun</p></li>
                            <li><p class="datetext day">Mon</p></li>
                            <li><p class="datetext day">Tue</p></li>
                            <li><p class="datetext day">Wed</p></li>
                            <li><p class="datetext day">Thur</p></li>
                            <li><p class="datetext day">Fri</p></li>
                            <li><p class="datetext holiday">Sat</p></li>
                        </ol>
                        </li>
                    <%
                        
                       
                                         int month = (engmonth.SelectedIndex) + 1;
                                         int year = Convert.ToInt32(engyear.SelectedValue);
                                         int totaldays = getNepTotalDay(Convert.ToInt32(nepmonth.SelectedIndex) + 1, Convert.ToInt32(nepyear.SelectedValue));
                                         int day = 1;
                                         int date = getEngDay(month, year);

                                       //  Response.Write(month);
                                       //  Response.Write(year);
                                       //  Response.Write(date);
                                         int ft = 0;
                                         if (date != 0 && totaldays != 0 && month != 0)
                                         {
                                             for (int i = 1; i <= 6; i++)
                                             {
                                                 for (int j = 1; j <= 7; )
                                                 {
                                                     if (ft == 0)
                                                     {
                                                         Response.Write("<li id='lastmonth'>");
                                                         Response.Write("<ol>");
                                                         for (int k = 1; k < date; k++)
                                                         {
                                                             Response.Write("<li>" + "\t" + "</li>");
                                                             j++;
                                                         }
                                                         Response.Write("</ol>");
                                                         Response.Write("</li>");
                                                         ft = 1;
                                                     }
                                                     Response.Write("<li id='thismonth'>");
                                                     Response.Write("<ol>");

                                                     if (day <= totaldays)
                                                     {
                                                         if (day != Convert.ToInt32(nepdate.SelectedItem.ToString()))
                                                         {
                                                             Response.Write("<li>" + getCorresEngDate(day) + "<p class = 'datetext'>" + NepDateMap[day] + "</p></li>");
                                                             day++;
                                                         }
                                                         else
                                                         {
                                                             Response.Write("<li class = 'thatdate'>" + getCorresEngDate(day) + "<p class = 'datetext'>" + NepDateMap[day] + "</p></li>");
                                                             day++;
                                                         }
                                                     }
                                                     j++;
                                                     Response.Write("</ol>");
                                                 }


                                                 Response.Write("</li>");
                                             }
                                         }
                    %>
                </ol>

                </div>  
                <%
                    button2 = 0;
                    } %>


<div class = "resultpg grid_12">
<asp:Label ID="v1" runat="server" class = "grid_6 push_3">(This is a community preview) <br />
(Converts date from 14/4/1943 to 15/4/2034 AD) </asp:Label>
<div class="clear"></div>
</div>
	

</div>

<div id="footer_content">
<div style="padding: 5px; background-color: rgb(102, 153, 255);"><strong>social</strong></div><!--socail  icons "Deepankar"-->
								 
<table>
  <tbody>
  <tr>
	<td>&nbsp;<img src="facebookicon_24.png" style="width: 24px; height: 24px;" alt=""></td>
										<td>&nbsp;<a target="_blank" href="https://www.facebook.com/#%21/home.php?sk=group_204207989619087&amp;ap=1#">MIC Nepal Fan Page&nbsp;</a></td>
								   
										<td>&nbsp;<img src="twittericon_24.png" alt=""></td>
										<td>&nbsp;<a target="_blank" href="https://twitter.com/microsoftMIC">MIC Nepal Twitter&nbsp;</a></td></tr></tbody></table><!--socail  icons "Deepankar"--> 
						<!--  <a href="http://go.microsoft.com/?linkid=8786242&lcid=3081">Manage Your Profile">Manage Your Profile All rights reserved.  <a href="http://go.microsoft.com/?linkid=4412892">Terms of Use</a>   |  <a href="http://www.microsoft.com/library/toolbar/3.0/trademarks/en-us.mspx">Trademarks</a>   |  <a href="http://privacy.microsoft.com/en-us/default.mspx">Privacy Statement</a> --> 
				 </div>

 <div id="footer_logo"><a href="#"> <img alt="Microsoft Innovation Center, Nepal" src="logo_ms.gif" height="67" width="361"></a></div></div>
</body></html>