﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        static bool IsFirstTime = false;
        public int fdate;
        int fEtotaldays = 0;
        int Emonth = 0;
        int Eyr = 0;
        public int button1 = 0;
        public int button2 = 0;
        public int fNtotaldays = 0;
        public Dictionary<String, String> EngMonthMap = new Dictionary<String, String>();
        public Dictionary<int, String> NepDateMap = new Dictionary<int, String>();

        protected void Page_Load(object sender, EventArgs e)
        {
            error.Text = "";
            rslt.Text = "";
            if (!Page.IsPostBack)
            {
                for (int i = 1; i <= 31; i++)
                {
                    engdate.Items.Add(i.ToString());

                }

                for (int i = 1; i <= 32; i++)
                {
                    nepdate.Items.Add(i.ToString());

                }

                for (int k = 1943; k <= 2034; k++)
                {
                    engyear.Items.Add(k.ToString());

                }


                for (int j = 2000; j <= 2090; j++)
                {
                    nepyear.Items.Add(j.ToString());

                }
                engyear.Items.FindByValue("2012").Selected = true;
                nepyear.Items.FindByValue("2069").Selected = true;


                IsFirstTime = true;
            }

            NepDateMap.Add(1, "१");
            NepDateMap.Add(2, "२");
            NepDateMap.Add(3, "३");
            NepDateMap.Add(4, "४");
            NepDateMap.Add(5, "५");
            NepDateMap.Add(6, "६");
            NepDateMap.Add(7, "७");
            NepDateMap.Add(8, "८");
            NepDateMap.Add(9, "९");
            NepDateMap.Add(10, "१०");
            NepDateMap.Add(11, "११");
            NepDateMap.Add(12, "१२");
            NepDateMap.Add(13, "१३");
            NepDateMap.Add(14, "१४");
            NepDateMap.Add(15, "१५");
            NepDateMap.Add(16, "१६");
            NepDateMap.Add(17, "१७");
            NepDateMap.Add(18, "१८");
            NepDateMap.Add(19, "१९");
            NepDateMap.Add(20, "२०");
            NepDateMap.Add(21, "२१");
            NepDateMap.Add(22, "२२");
            NepDateMap.Add(23, "२३");
            NepDateMap.Add(24, "२४");
            NepDateMap.Add(25, "२५");
            NepDateMap.Add(26, "२६");
            NepDateMap.Add(27, "२७");
            NepDateMap.Add(28, "२८");
            NepDateMap.Add(29, "२९");
            NepDateMap.Add(30, "३०");
            NepDateMap.Add(31, "३१");
            NepDateMap.Add(32, "३२");

            EngMonthMap.Add("Baishakh", "Apr-May");
            EngMonthMap.Add("Jestha", "May-June");
            EngMonthMap.Add("Asadh", "June-July");
            EngMonthMap.Add("Shrawan", "July-Aug");
            EngMonthMap.Add("Bhadra", "Aug-Sept");
            EngMonthMap.Add("Aswin", "Sept-Oct");
            EngMonthMap.Add("Kartik", "Oct-Nov");
            EngMonthMap.Add("Mangsir", "Nov-Dec");
            EngMonthMap.Add("Poush", "Dec-Jan");
            EngMonthMap.Add("Magh", "Jan-Feb");
            EngMonthMap.Add("Falgun", "Feb-Mar");
            EngMonthMap.Add("Chaitra", "Mar-Apr");

        }


        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void engmonth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                String result;

                Neptoeng test = new Neptoeng();
                result = test.DateConversion(Convert.ToInt32(nepdate.SelectedValue), Convert.ToInt32(nepmonth.SelectedValue), Convert.ToInt32(nepyear.SelectedValue));

                rslt.Text = result;
                fEtotaldays = test.getTotalDay();
                Emonth = test.getmonth();
                Eyr = test.getyear();
                String Edate = test.getdate();
                Neptoeng test2 = new Neptoeng();
                
                String s = test2.DateConversion(1, Convert.ToInt32(nepmonth.SelectedValue), Convert.ToInt32(nepyear.SelectedValue));
                fdate = test2.getDayOW();
                button1 = 1;


                engyear.SelectedIndex = engyear.Items.IndexOf(engyear.Items.FindByText(Eyr.ToString()));
                engmonth.SelectedIndex = engmonth.Items.IndexOf(engmonth.Items.FindByValue((Emonth).ToString()));
                engdate.SelectedIndex = engdate.Items.IndexOf(engdate.Items.FindByText(Edate.ToString()));

            }
            catch
            {
                error.Text = "Your input value is incorrect. Please check date again";
            }


            if (IsPostBack)
            {
                this.ClientScript.RegisterStartupScript(this.GetType(), "show", "<script>postToFeed(); </script>");
            }
            else
            {
                this.ClientScript.RegisterStartupScript(this.GetType(), "show", "<script>document.getElementById('facebook-jssdk').style.display = 'hidden'</script>");
            }
        }

        protected void Submit_Click2(object sender, EventArgs e)
        {
            try
            {
                String result;
                int fNdate, Nmonth, Nyr;

                EngNep rslt2 = new EngNep();
                result = rslt2.Nepdate(Convert.ToInt32(engdate.SelectedValue), Convert.ToInt32(engmonth.SelectedValue), Convert.ToInt32(engyear.SelectedValue));

                rslt.Text = result;

                fNdate = rslt2.getdate();
                Nmonth = rslt2.getmonth();
                Nyr = rslt2.getyear();

                nepyear.SelectedIndex = nepyear.Items.IndexOf(nepyear.Items.FindByText(Nyr.ToString()));
                nepmonth.SelectedIndex = nepmonth.Items.IndexOf(nepmonth.Items.FindByValue((Nmonth).ToString()));
                nepdate.SelectedIndex = nepdate.Items.IndexOf(nepdate.Items.FindByText(fNdate.ToString()));

                Neptoeng test2 = new Neptoeng();
                String s = test2.DateConversion(1, Convert.ToInt32(nepmonth.SelectedValue), Convert.ToInt32(nepyear.SelectedValue));
                fdate = test2.getDayOW();
               
                button2 = 1;

            }
            catch
            {
                error.Text = "Your input value is incorrect. Please check date again";

            }
        }
        public int getEngDay(int mon, int yr)
        {

            return fdate;
        }

        public int getEngTotalDay()
        {
            return (fEtotaldays);
        }

        public int getNepTotalDay(int m, int yr)
        {
            EngNep rslt2 = new EngNep();
            fNtotaldays = rslt2.getTotalDay(m, yr);
            return (fNtotaldays);
        }

        public int getEngMonth()
        {
            return (Emonth);
        }

        public int getEngYear()
        {
            return (Eyr);
        }
        public String getCorresEngDate(int day)
        {
            Neptoeng test = new Neptoeng();
            String result = test.DateConversion(day, Convert.ToInt32(nepmonth.SelectedValue), Convert.ToInt32(nepyear.SelectedValue));
            return test.getdate();
        }

        public String GetEngMonthD(int n)
        {
            return (nepmonth.Items.FindByValue((n + 1).ToString()).Text);
        }

    }
}